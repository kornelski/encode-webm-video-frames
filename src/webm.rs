use std::io::{Write, Seek};
use std::error::Error;

use crate::vpxencoder::*;
use crate::config::*;
use crate::webm_native::mux;
use crate::webm_native::mux::Track;

/// WebM video encoder
pub struct WebM<M> {
    segment: Option<mux::Segment<M>>,
    track: Option<mux::VideoTrack>,
}

impl<W: Write + Seek> WebM<mux::Writer<W>> {
    /// The writer can be an opened `File` (it has to support seeking) or a `Vec`
    pub fn new(writer: W) -> Self {
        WebM {
            segment: mux::Segment::new(mux::Writer::new(writer)),
            track: None,
        }
    }
}

impl<W: Send> VPXWriter for WebM<W> {
    fn start_frames(&mut self, config: &Config, _frame_cnt: Option<u32>) -> Result<(), Box<dyn Error + Send + Sync>> {
        let segment = self.segment.as_mut().ok_or("webmwriter was unable to create webm segment")?;
        segment.set_app_name("Rust");

        let mut track = segment.add_video_track(config.inner.g_w, config.inner.g_h, Some(1), mux::VideoCodecId::VP9);

        if !track.set_color(8, (true, true), false) {
            Err("webm writer can't set color")?;
        }

        self.track = Some(track);
        Ok(())
    }

    fn write_frame(&mut self, config: &Config, pts: u64, data: &[u8], keyframe: bool) -> Result<(), Box<dyn Error + Send + Sync>> {
        let timestamp_ns = 1_000_000_000_u64 * pts * config.inner.g_timebase.num as u64 / config.inner.g_timebase.den as u64;

        let track = self.track.as_mut().ok_or("bad webm writer state")?;
        if !track.add_frame(data, timestamp_ns, keyframe) {
            Err("webm writer can't add frame")?;
        }
        Ok(())
    }

    fn finalize(&mut self, _frames_encoded: u32) -> Result<(), Box<dyn Error + Send + Sync>> { // config: &Config,
        if let Some(segment) = self.segment.take() {
            let res = segment.finalize(None);
            self.track = None;
            if !res {
                Err("webm writer can't finalize")?
            }
        }
        Ok(())
    }
}
