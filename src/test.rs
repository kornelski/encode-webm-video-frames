use std::error::Error;
use crate::config::*;
use crate::ivf::*;
use crate::vpxencoder::*;
use std::io;
use imgref::*;

#[test]
fn test_ffi() -> Result<(), Box<dyn Error + Send + Sync>> {
    let width = 16;
    let height = 16;
    let chroma_width = width as usize/2;
    let chroma_height = height as usize/2;

    let mut y = vec![0u8; width as usize * height as usize];
    let mut u = vec![0u8; chroma_width * chroma_height];
    let mut v = vec![0u8; chroma_width * chroma_height];

    let cfg = Config::vp9_enc(width, height, 10, None, 128).unwrap();

    let total_frame_count = 100;
    let outfile = io::Cursor::new(Vec::new());
    let writer = IVF::new(outfile);
    let mut ctx = VPXEncoder::new(cfg, writer)?;
    ctx.start(None)?;

    let mut frame_num = 0;
    loop {
        for y in y.iter_mut() {
            *y = y.wrapping_add(frame_num as u8);
        }
        for (n, (u,v)) in u.iter_mut().zip(v.iter_mut()).enumerate() {
            *u = u.wrapping_add((frame_num as usize + n) as u8);
            *v = v.wrapping_add((100-frame_num as usize + n) as u8);
        }
        ctx.vpx_encode_frame(Img::new(&y, width, height), &u, &v, frame_num, 1)?;
        frame_num += 1;
        if frame_num >= total_frame_count as u64 {
            break;
        }
    }
    Ok(())
}
