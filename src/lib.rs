///! ```rust,ignore
///  let mut writer = WebM::new(File::create(filename)?);
///! let mut cfg = Config::vp9_enc(dest_width, dest_height, framerate, quality, bitrate)?;
///! let mut encoder = VPXEncoder::new(cfg, writer)?;
///! encoder.start(Some(expected_num_frames))?;
///! ```

extern crate vpx_sys as ffi;
extern crate webm as webm_native;

mod ivf;
mod vpxencoder;
mod config;
mod image;
mod webm;

#[cfg(test)]
mod test;

pub use crate::ivf::*;
pub use crate::webm::*;
pub use crate::vpxencoder::*;
pub use crate::config::*;
pub use crate::image::*;
