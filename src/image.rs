use crate::ffi::*;
use imgref::*;
use std::error::Error;
use std::default::Default;
use std::ptr;
use std::marker::PhantomData;

/// A single frame
pub struct VPXImage<'pixels> {
    pub inner: vpx_image_t,
    _outlive_given_pixels: PhantomData<&'pixels [u8]>,
}

impl<'pixels> VPXImage<'pixels> {
    /// This expects planar chroma-subsampled YCbCr image in BT 601 color space
    ///
    /// Additionally, libvpx wants even image dimensions.
    pub fn new_yuv420(y: ImgRef<'pixels, u8>, u: &'pixels [u8], v: &'pixels [u8]) -> Result<Self, Box<dyn Error + Send + Sync>> {
        let width = y.width();
        let height = y.height();
        let luma_stride = y.stride();
        let chroma_width = (width+1)/2;
        let chroma_height = (height+1)/2;

        assert!(y.buf.len() >= height * luma_stride, "bad buf size");
        assert_eq!(u.len(), chroma_width * chroma_height, "bad u len");
        assert_eq!(v.len(), chroma_width * chroma_height, "bad v len");

        let mut inner = Default::default();
        unsafe {
            let image_ptr = vpx_img_wrap(&mut inner,
                vpx_img_fmt::VPX_IMG_FMT_I420,
                width as u32, height as u32, 1,
                // img_wrap will assume the data is for 3 planes, rather than just 1, but this will be corrected
                y.buf.as_ptr() as *mut _ // FFI has needless mut here
            );
            if image_ptr.is_null() {
                Err("Failed to allocate image.")?;
            }

            // It shouldn't create a new handle, just fill in the old one
            assert_eq!(image_ptr, (&mut inner) as *mut _, "bad ptr");
            assert_eq!(inner.img_data, y.buf.as_ptr() as *mut _, "bad img ptr");

            // Hide full image data in case something tries to use it. Only planes will be configured.
            inner.img_data = ptr::null_mut();

            // wrap should have figured out the expected stride
            inner.stride[VPX_PLANE_Y as usize] = luma_stride as i32;
            inner.stride[VPX_PLANE_U as usize] = chroma_width as i32;
            inner.stride[VPX_PLANE_V as usize] = chroma_width as i32;

            inner.planes[VPX_PLANE_Y as usize] = y.buf.as_ptr() as *mut _; // FFI has needless mut here
            inner.planes[VPX_PLANE_U as usize] = u.as_ptr() as *mut _;
            inner.planes[VPX_PLANE_V as usize] = v.as_ptr() as *mut _;
        }

        inner.cs = vpx_color_space::VPX_CS_BT_601;

        Ok(VPXImage {
            inner: inner,
            _outlive_given_pixels: PhantomData,
        })
    }

    pub fn plane_width(&self, plane: usize) -> usize {
        if plane > 0 && self.inner.x_chroma_shift > 0 {
            (self.inner.d_w as usize + 1) >> self.inner.x_chroma_shift as usize
        } else {
            self.inner.d_w as usize
        }
    }

    pub fn plane_height(&self, plane: usize) -> usize {
        if plane > 0 && self.inner.y_chroma_shift > 0 {
            (self.inner.d_h as usize + 1) >> self.inner.y_chroma_shift as usize
        } else {
            self.inner.d_h as usize
        }
    }
}

impl<'pixels> Drop for VPXImage<'pixels> {
    fn drop(&mut self) {
        unsafe {
            // It doesn't do anything currently, but keep it just in case libvpx adds new data
            vpx_img_free(&mut self.inner);
        }
    }
}
