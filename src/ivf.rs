use std::io::{Write, Seek, SeekFrom};
use byteorder::{WriteBytesExt, LittleEndian as LE};
use std::error::Error;

use crate::vpxencoder::*;
use crate::config::*;

/// Writer for the primitive IVF format
pub struct IVF<W: Write + Seek> {
    writer: W,
}

impl<W: Write + Seek> IVF<W> {
    pub fn new(writer: W) -> Self {
        IVF {
            writer: writer,
        }
    }
}

impl<W: Write + Seek + Send> VPXWriter for IVF<W> {
    fn start_frames(&mut self, config: &Config, frame_cnt: Option<u32>) -> Result<(), Box<dyn Error + Send + Sync>> {
        self.writer.write_all("DKIF".as_bytes())?;
        self.writer.write_u16::<LE>(0)?;                     // version
        self.writer.write_u16::<LE>(32)?;                    // header size
        self.writer.write_u32::<LE>(VP9_FOURCC)?;
        self.writer.write_u16::<LE>(config.inner.g_w as u16)?;             // width
        self.writer.write_u16::<LE>(config.inner.g_h as u16)?;             // height
        self.writer.write_u32::<LE>(config.inner.g_timebase.den as u32)?;  // rate
        self.writer.write_u32::<LE>(config.inner.g_timebase.num as u32)?;  // scale
        self.writer.write_u32::<LE>(frame_cnt.unwrap_or(0))?; // length
        self.writer.write_u32::<LE>(0)?;                      // unused
        Ok(())
    }

    fn finalize(&mut self, frames_encoded: u32) -> Result<(), Box<dyn Error + Send + Sync>> {
        self.writer.seek(SeekFrom::Start(24))?;
        self.writer.write_u32::<LE>(frames_encoded)?;
        Ok(())
    }

    fn write_frame(&mut self, _config: &Config, pts: u64, data: &[u8], _key: bool) -> Result<(), Box<dyn Error + Send + Sync>> {
        self.writer.write_u32::<LE>(data.len() as u32)?;
        self.writer.write_u64::<LE>(pts)?;
        self.writer.write_all(data)?;
        Ok(())
    }
}
