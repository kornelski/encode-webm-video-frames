use crate::ffi::*;
use std::error::Error;
use std::default::Default;
use std::ffi::CStr;

///  VPX encoder configuration
pub struct Config {
    pub inner: vpx_codec_enc_cfg_t,
    pub iface: *mut vpx_codec_iface_t,
    q_target: u32,
    sharp: bool,
}

impl Config {
    /// Configure for VP9 one-pass encoding
    ///
    /// Quality is target quality and max quality in 0..100 range.
    pub fn vp9_enc(width: usize, height: usize, fps: u16, quality: Option<(u32, u32)>, bitrate_kbps: u32) -> Result<Self, Box<dyn Error>> {
        let mut inner = Default::default();

        unsafe {
            let iface = vpx_codec_vp9_cx();

            let res = vpx_codec_enc_config_default(iface, &mut inner, 0);
            if res != VPX_CODEC_OK {
                Err(format!("Failed to get default codec config. {:?}", res))?;
            }

            inner.g_w = width as u32;
            inner.g_h = height as u32;

            let mut config = Config {
                sharp: false,
                iface: iface,
                inner: inner,
                q_target: 0,
            };
            config.set_fps(fps);
            config.set_bitrate_kbps(bitrate_kbps);
            if let Some((std, max)) = quality {
                config.set_quality(std, max);
            }

            Ok(config)
        }
    }

    pub fn set_sharpness(&mut self, is_sharp: bool) {
        self.sharp = is_sharp;
    }
    pub fn sharpness(&self) -> i32 {
        if self.sharp {1} else {0}
    }

    pub fn max_encoding_time_s(&self) -> u32 {
        5
    }

    pub fn codec_name(&self) -> &str {
        unsafe {
            CStr::from_ptr(vpx_codec_iface_name(self.iface)).to_str().unwrap_or("")
        }
    }

    pub fn set_bitrate_kbps(&mut self, bitrate: u32) {
        self.inner.rc_target_bitrate = bitrate;
    }

    pub fn set_fps(&mut self, fps: u16) {
        self.inner.g_timebase.num = 1;
        self.inner.g_timebase.den = fps.into();
    }

    pub fn cq_level(&self) -> Option<u32> {
        if self.inner.rc_end_usage == vpx_rc_mode::VPX_CQ {
            Some(63 * (100-self.q_target) / 100)
        } else {
            None
        }
    }

    /// Target quality and max quality in 0..100 range (VPX's `rc_min_quantizer`)
    pub fn set_quality(&mut self, target: u32, max_q: u32) {
        assert!(target <= 100);
        assert!(max_q <= 100);
        assert!(target <= max_q);

        self.q_target = target;
        self.inner.rc_end_usage = vpx_rc_mode::VPX_CQ;
        self.inner.g_pass = vpx_enc_pass::VPX_RC_ONE_PASS;
        self.inner.rc_min_quantizer = self.inner.rc_max_quantizer * (100 - max_q) / 100;
    }
}
