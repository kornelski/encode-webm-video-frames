use crate::ffi::*;
use imgref::*;
use std::error::Error;
use std::default::Default;
use std::ptr;
use std::cmp;
use std::slice;
use crate::config::*;
use crate::image::*;
use std::time::Instant;

/// See individual encoder implementations for details
pub trait VPXWriter: Send {
    /// Initialize encoder. Hint number of frames for more efficient writing.
    fn start_frames(&mut self, config: &Config, frame_cnt: Option<u32>) -> Result<(), Box<dyn Error + Send + Sync>>;
    /// Add a frame.
    ///
    /// `pts` is presentation timestamp whne the frame is supposed to be displayed. data is libvpx packet, key if it's a keyframe
    fn write_frame(&mut self, config: &Config, pts: u64, data: &[u8], _key: bool) -> Result<(), Box<dyn Error + Send + Sync>>;

    /// Finish writing, update number of frames in the file
    fn finalize(&mut self, frames_encoded: u32) -> Result<(), Box<dyn Error + Send + Sync>>;
}

enum State {
    New,
    WritingFrames,
    Finalized,
}

/// VP9 encoder for use with WebM
pub struct VPXEncoder<W: VPXWriter> {
    pub inner: vpx_codec_ctx_t,
    writer: W,
    pub config: Config,
    finalized: bool,
    frames_in_header: u32,
    frames_added: u32,
    frames_encoded: u32,
    approx_frames_total: Option<u32>,
    current_cpu_tradeoff: i32,
    max_encoding_time_s: u32,
    encoding_start: Instant,
    state: State,
}

pub const VP8_FOURCC: u32 = 0x30385056;
pub const VP9_FOURCC: u32 = 0x30395056;

unsafe impl<W: VPXWriter + Send> Send for VPXEncoder<W> {}

impl<W: VPXWriter> VPXEncoder<W> {
    /// Start writing to a file (make writer from `WebM`)
    pub fn new(config: Config, writer: W) -> Result<Self, Box<dyn Error + Send + Sync>> {
        let mut inner = Default::default();

        let res = unsafe {
            vpx_codec_enc_init_ver(&mut inner,
                                   config.iface,
                                   &config.inner,
                                   0,
                                   VPX_ENCODER_ABI_VERSION as i32)
        };
        if res != VPX_CODEC_OK {
            Err(format!("Codec init fail {:?}", res))?;
        }

        // Not sure if it helps?
        if let Some(cq_level) = config.cq_level() {
            let res = unsafe {
                vpx_codec_control_(&mut inner, vp8e_enc_control_id::VP8E_SET_CQ_LEVEL as i32, cq_level)
            };
            if res != VPX_CODEC_OK {
                Err("vpx_codec_control_ failed")?;
            }
        }

        unsafe {
            vpx_codec_control_(&mut inner, vp8e_enc_control_id::VP8E_SET_SHARPNESS as i32, config.sharpness());
            vpx_codec_control_(&mut inner, vp8e_enc_control_id::VP9E_SET_GF_CBR_BOOST_PCT as i32, 400);
            vpx_codec_control_(&mut inner, vp8e_enc_control_id::VP9E_SET_NOISE_SENSITIVITY as i32, 0);
            vpx_codec_control_(&mut inner, vp8e_enc_control_id::VP9E_SET_COLOR_SPACE as i32, vpx_color_space::VPX_CS_BT_601);
        }

        let max_encoding_time_s = config.max_encoding_time_s();

        Ok(VPXEncoder {
            state: State::New,
            config: config,
            writer: writer,
            inner: inner,
            frames_added: 0,
            frames_encoded: 0,
            frames_in_header: 0,
            approx_frames_total: None,
            current_cpu_tradeoff: 0,
            max_encoding_time_s: max_encoding_time_s,
            encoding_start: Instant::now(),
            finalized: false,
        })
    }

    /// Begin encoding.
    ///
    /// You can hint what the total number of frames will be
    pub fn start(&mut self, frame_cnt: Option<u32>) -> Result<(), Box<dyn Error + Send + Sync>> {
        match self.state {
            State::New => {},
            _ => Err("bad state")?
        }

        self.writer.start_frames(&self.config, frame_cnt)?;

        self.approx_frames_total = frame_cnt;
        self.frames_in_header = frame_cnt.unwrap_or(0);
        self.state = State::WritingFrames;
        Ok(())
    }

    fn update_frame_count(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
        match self.state {
            State::Finalized => {},
            _ => Err("bad state")?,
        }

        if self.frames_in_header != self.frames_encoded {
            self.writer.finalize(self.frames_encoded)?;
        }
        Ok(())
    }

    fn set_cpu_tradeoff(&mut self, value: i32) {
        if value != self.current_cpu_tradeoff {
            self.current_cpu_tradeoff = value;
            unsafe {
                vpx_codec_control_(&mut self.inner, vp8e_enc_control_id::VP8E_SET_CPUUSED as i32, value);
            }
        }
    }

    fn write_packets(&mut self) -> Result<bool, Box<dyn Error + Send + Sync>> {
        match self.state {
            State::WritingFrames => {},
            _ => {
                return Err("bad state")?;
            },
        }

        let mut got_pkts = false;
        let mut iter = ptr::null();
        unsafe { loop {
            let packet = vpx_codec_get_cx_data(&mut self.inner, &mut iter);
            if packet.is_null() {
                break;
            }
            got_pkts = true;
            if (*packet).kind == vpx_codec_cx_pkt_kind::VPX_CODEC_CX_FRAME_PKT {
                let frame = &(*packet).data.frame;
                assert!(frame.pts >= 0, "bad pts");
                assert!(frame.duration > 0, "bad duration");
                assert_eq!(0, frame.flags & VPX_FRAME_IS_INVISIBLE, "bad flag");

                let data = slice::from_raw_parts(frame.buf as *const _, frame.sz);
                let keyframe = (frame.flags & VPX_FRAME_IS_KEY) != 0;
                self.writer.write_frame(&self.config, frame.pts as u64, data, keyframe)?;
                self.frames_encoded += 1;
            }
        }}
        Ok(!got_pkts)
    }

    /// Encode frame in YCbCr format with 420 chroma subsampling and BT 601 color space
    ///
    /// Encoding is designed for speed, and will lower quality if encoding takes more than a few seconds
    pub fn vpx_encode_frame(&mut self, y: ImgRef<'_, u8>, cb: &[u8], cr: &[u8], frame_index: u64, duration: u64) -> Result<(), Box<dyn Error + Send + Sync>> {
        let img = VPXImage::new_yuv420(y, cb, cr)?;

        match self.state {
            State::WritingFrames => {},
            _ => Err("bad state")?,
        }

        let flags = if 0 == self.frames_added {
            VPX_EFLAG_FORCE_KF as i64
        } else {0};

        let tradeoff = self.cpu_tradeoff_for_current_time();
        self.set_cpu_tradeoff(tradeoff as i32);

        let quality_tradeoff = self.quality_tradeoff_value(tradeoff);

        let res = unsafe {
            vpx_codec_encode(&mut self.inner, &img.inner, frame_index as i64, duration, flags, quality_tradeoff.into())
        };
        if res != VPX_CODEC_OK {
            Err(format!("Failed to encode frame {:?}", res))?
        }
        self.frames_added += 1;
        self.write_packets()?;
        Ok(())
    }

    fn cpu_tradeoff_for_current_time(&self) -> i32 {
        let frames = cmp::max(self.frames_added, self.approx_frames_total.unwrap_or(0));
        let elapsed = self.encoding_start.elapsed().as_secs() as i32;

        let tradeoff = match frames {
            0 ..= 4   => 0,
            0 ..= 9   => 1,
            0 ..= 14  => 2,
            0 ..= 25  => 3,
            0 ..= 80  => 4,
            0 ..= 200 => 5,
                    _ => 6,
        };
        cmp::min(8, cmp::max(tradeoff, 4 * elapsed / self.max_encoding_time_s as i32))
    }

    fn quality_tradeoff_value(&self, cpu_tradeoff: i32) -> u32 {
        // Encode first few frames with slow encoding (for gifs with static bg), but slow isn't acceptable for long gifs
        if cpu_tradeoff <= 4 || (cpu_tradeoff < 6 && self.frames_added < 10) {
            VPX_DL_GOOD_QUALITY
        } else {
            VPX_DL_GOOD_QUALITY / (cpu_tradeoff as u32)
        }
    }

    /// Finish writing (for short videos, it may be just beginning the writing)
    pub fn vpx_finalize(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
        match self.state {
            State::New | State::Finalized => return Ok(()),
            _ => {
                while !self.finalized {
                    let quality_tradeoff = self.quality_tradeoff_value(self.current_cpu_tradeoff);
                    let res = unsafe {
                        vpx_codec_encode(&mut self.inner, ptr::null_mut(), -1, 1, 0, quality_tradeoff.into())
                    };
                    if res != VPX_CODEC_OK {
                        Err("Failed to flush frames")?
                    }

                    if self.write_packets()? {
                        self.state = State::Finalized;
                        self.finalized = true;
                    }
                }

                self.update_frame_count()
            },
        }
    }
}

impl<W: VPXWriter> Drop for VPXEncoder<W> {
    fn drop(&mut self) {
        self.vpx_finalize().ok(); // Can't panic here
        unsafe {
            vpx_codec_destroy(&mut self.inner);
        }
    }
}
