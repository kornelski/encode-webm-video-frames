# Simple video encoder using libvpx

This encoder has been written as a back-end [for a GIF to video converter](https://imageoptim.com/api/ungif), so it's optimized for that purpose only.

It can write WebM files, or for testing purposes, IVF files.

Please note that this encoder only takes frames in subsampled (420) YUV format. It does not support RGB, so you'll have to perform the conversion yourself.

----

It's a Google-style throw-over-the-wall open-source release, so there's no support.
